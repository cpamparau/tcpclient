﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MyTcpClient::Start()
extern void MyTcpClient_Start_m104CCFC36AB3809BF618EC83F25A6D59962423D2 (void);
// 0x00000002 System.Void MyTcpClient::Connect(System.String,System.String)
extern void MyTcpClient_Connect_mAF3B89BF8EE1D773EB422832BB0F67C008CE2DF7 (void);
// 0x00000003 System.Void MyTcpClient::ConnectUWP(System.String,System.String)
extern void MyTcpClient_ConnectUWP_mEA8D3BC36E26912BA376562F1E72B4591E701E4C (void);
// 0x00000004 System.Void MyTcpClient::ConnectUnity(System.String,System.String)
extern void MyTcpClient_ConnectUnity_mF09AD27B96C8F01C020F4B623D2ABC9D8B3B6304 (void);
// 0x00000005 System.Void MyTcpClient::RestartExchange()
extern void MyTcpClient_RestartExchange_m58CAC58FEA263B3BBA1FFB6A98B2EA4749959EC2 (void);
// 0x00000006 System.Void MyTcpClient::Update()
extern void MyTcpClient_Update_mD23DFC56AA51AD72AD4892B20692E61B64087C42 (void);
// 0x00000007 System.Void MyTcpClient::ExchangePackets()
extern void MyTcpClient_ExchangePackets_m2A6F7F74C8782C0D14769BB6E117F3A2E237BACE (void);
// 0x00000008 System.Void MyTcpClient::ReportDataToTrackingManager(System.String)
extern void MyTcpClient_ReportDataToTrackingManager_m300B7FEF9550885FF2B07F59F9F38379898B3C77 (void);
// 0x00000009 System.Void MyTcpClient::ReportStringToTrackingManager(System.String)
extern void MyTcpClient_ReportStringToTrackingManager_m4AA27624BD1FE8A32D421F9CEAA5E54E6AD8276D (void);
// 0x0000000A System.Void MyTcpClient::StopExchange()
extern void MyTcpClient_StopExchange_m6F435339B226C6938B1F4887D594161125A32022 (void);
// 0x0000000B System.Void MyTcpClient::OnDestroy()
extern void MyTcpClient_OnDestroy_mA420B33A7481108A33D683F026CCE8876B75D851 (void);
// 0x0000000C System.Void MyTcpClient::.ctor()
extern void MyTcpClient__ctor_m469CFEDEF07B38E89BCBC0B04B6D2A9F1A4A73A6 (void);
// 0x0000000D System.Void MyTcpClient::<RestartExchange>b__19_0()
extern void MyTcpClient_U3CRestartExchangeU3Eb__19_0_m2756FCFF51168982D24B070D0863FA7C89402BD9 (void);
// 0x0000000E System.Void MyTcpClient/<ConnectUWP>d__10::MoveNext()
extern void U3CConnectUWPU3Ed__10_MoveNext_m43B13157D4704BF2F1629105DF0515B9A28066E8 (void);
// 0x0000000F System.Void MyTcpClient/<ConnectUWP>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConnectUWPU3Ed__10_SetStateMachine_m1FA908D62564675B32BF9402F638EB010CC06BC3 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	MyTcpClient_Start_m104CCFC36AB3809BF618EC83F25A6D59962423D2,
	MyTcpClient_Connect_mAF3B89BF8EE1D773EB422832BB0F67C008CE2DF7,
	MyTcpClient_ConnectUWP_mEA8D3BC36E26912BA376562F1E72B4591E701E4C,
	MyTcpClient_ConnectUnity_mF09AD27B96C8F01C020F4B623D2ABC9D8B3B6304,
	MyTcpClient_RestartExchange_m58CAC58FEA263B3BBA1FFB6A98B2EA4749959EC2,
	MyTcpClient_Update_mD23DFC56AA51AD72AD4892B20692E61B64087C42,
	MyTcpClient_ExchangePackets_m2A6F7F74C8782C0D14769BB6E117F3A2E237BACE,
	MyTcpClient_ReportDataToTrackingManager_m300B7FEF9550885FF2B07F59F9F38379898B3C77,
	MyTcpClient_ReportStringToTrackingManager_m4AA27624BD1FE8A32D421F9CEAA5E54E6AD8276D,
	MyTcpClient_StopExchange_m6F435339B226C6938B1F4887D594161125A32022,
	MyTcpClient_OnDestroy_mA420B33A7481108A33D683F026CCE8876B75D851,
	MyTcpClient__ctor_m469CFEDEF07B38E89BCBC0B04B6D2A9F1A4A73A6,
	MyTcpClient_U3CRestartExchangeU3Eb__19_0_m2756FCFF51168982D24B070D0863FA7C89402BD9,
	U3CConnectUWPU3Ed__10_MoveNext_m43B13157D4704BF2F1629105DF0515B9A28066E8,
	U3CConnectUWPU3Ed__10_SetStateMachine_m1FA908D62564675B32BF9402F638EB010CC06BC3,
};
extern void U3CConnectUWPU3Ed__10_MoveNext_m43B13157D4704BF2F1629105DF0515B9A28066E8_AdjustorThunk (void);
extern void U3CConnectUWPU3Ed__10_SetStateMachine_m1FA908D62564675B32BF9402F638EB010CC06BC3_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600000E, U3CConnectUWPU3Ed__10_MoveNext_m43B13157D4704BF2F1629105DF0515B9A28066E8_AdjustorThunk },
	{ 0x0600000F, U3CConnectUWPU3Ed__10_SetStateMachine_m1FA908D62564675B32BF9402F638EB010CC06BC3_AdjustorThunk },
};
static const int32_t s_InvokerIndices[15] = 
{
	2840,
	1301,
	1301,
	1301,
	2840,
	2840,
	2840,
	2323,
	2323,
	2840,
	2840,
	2840,
	2840,
	2840,
	2323,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	15,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
